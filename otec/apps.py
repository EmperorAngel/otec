from django.apps import AppConfig


class OtecConfig(AppConfig):
    name = 'otec'
