from django.db import models

# Create your models here.

class Tag(models.Model):
    id_tag = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=25,null=False)
    
    def __str__(self):
        return "Tag"

class Estado(models.Model):
    id_estado = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=25,null=False)
    def __str__(self):
        return "Estado"

class Certificacion(models.Model):
    id_certificacion = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=25,null=False)
    def __str__(self):
        return "Certificacion"

class Region(models.Model):
    id_region = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=25,null=False)

    def __str__(self):
        return "Region"


class Comuna(models.Model):
    id_comuna = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=25,null=False)
    region = models.ForeignKey(Region,on_delete=models.CASCADE)
    def __str__(self):
        return "COMUNA"


class Otec(models.Model):
    
    rut_otec = models.CharField(primary_key=True, max_length=35)
    nombre = models.CharField(max_length=50, null=False)
    direccion = models.CharField(max_length=100, null=False)
    telefono = models.CharField(max_length=50, null=False)
    email =models.EmailField(max_length=50, null=False)
    foto = foto = models.ImageField(upload_to="fotos/", null=True)
    comuna = models.ForeignKey(Comuna,on_delete=models.CASCADE)
    tag = models.ManyToManyField(Tag)
    

    def __str__(self):
        return "OTEC"

class Relator(models.Model):
    rut_relator = models.CharField(max_length=14, null=False,primary_key=True)
    nombre = models.CharField(max_length=50, null=False)
    apellido = models.CharField(max_length=50, null=True)
    telefono =  models.CharField(max_length=50, null=False)
    email = models.EmailField(max_length=50, null=False)
    estado = models.BooleanField('Activo',default=True)
    foto = foto = models.ImageField(upload_to="fotos/", null=True)
    estado_id_estado = models.ForeignKey(Estado,on_delete=models.CASCADE, blank=True, null=True)
    certificacion_id_certificacion = models.ForeignKey(Certificacion,on_delete=models.CASCADE,null=True)
    comuna = models.ForeignKey(Comuna,on_delete=models.CASCADE,null=True)
    tag = models.ManyToManyField(Tag)

    def __str__(self):
        return self.nombre  

class tipo_postulacion(models.Model):
    id_tipo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=25, null=False)
    def __str__(self):
        return "tipopostulacion"

class Curso(models.Model):
    id_curso = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100, null=False)
    documento_certificacion = models.FileField(upload_to="certificaciones/", null=True)
    certificacion = models.ForeignKey(Certificacion,on_delete=models.CASCADE)
    tag = models.ManyToManyField(Tag)

    def __str__(self):
        return "Curso"


class Publicacion(models.Model):
    id_publicacion = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=60,null=False)
    fecha_inicio = models.DateField(null=False)
    fecha_termino = models.DateField(null=False)
    sueldo = models.IntegerField(null=False)
    otec = models.ForeignKey(Otec,on_delete=models.CASCADE)
    curso = models.ForeignKey(Curso,on_delete=models.CASCADE)
    tag = models.ManyToManyField(Tag)


    def __str__(self):
        return "Publicacion"

class Asignacion(models.Model):
    fecha_inicio = models.DateField(null=False)
    fecha_termino = models.DateField(null=False)
    publicacion = models.ForeignKey(Publicacion,on_delete=models.CASCADE)
    relator = models.ForeignKey(Relator,on_delete=models.CASCADE)

    def __str__(self):
        return "Asignacion"


class Foto_Publicacion(models.Model):
    id_foto = models.IntegerField(primary_key=True)
    foto = models.ImageField(upload_to="fotos/",null=True)
    publicacion = models.ForeignKey(Publicacion,on_delete=models.CASCADE)

    def __str__(self):
        return "Foto_Publicacion"

class Invitaciones(models.Model):
    otec = models.ForeignKey(Otec,on_delete=models.CASCADE)
    relator = models.ForeignKey(Relator,on_delete=models.CASCADE)
    publicacion = models.ForeignKey(Publicacion,on_delete=models.CASCADE)
    tipo_postulacion= models.ForeignKey(tipo_postulacion,on_delete=models.CASCADE) 

    def __str__(self):
        return "Invitaciones"
    
    
class Empresa_Servicio(models.Model):
   rut_servicio = models.IntegerField(primary_key=True)
   nombre = models.CharField(max_length=70, null=False)
   direccion = models.CharField(max_length=70, null=False)
   telefono = models.CharField(max_length=70, null=True)
   email = models.EmailField(null=False)
   foto = models.ImageField(upload_to="fotos/", null=True)
   comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
   tag = models.ManyToManyField(Tag)
   def __str__(self):
        return "Empresa_servicio"
    
class Publicacion_Servicio(models.Model):
   id_pubser = models.IntegerField(primary_key=True)
   titulo = models.CharField(max_length=70,null=False)
   detalle = models.CharField(max_length=70,null=False)
   fecha = models.DateField(max_length=70,null=False)
   precio = models.EmailField(null=False)
   empresa_servicio_rut_servicio = models.ForeignKey(Empresa_Servicio,on_delete=models.CASCADE)
   def __str__(self):
        return "Publicacion_Servicio"


class Foto_Servicio(models.Model):
    id_foto = models.IntegerField(primary_key=True)
    foto = models.ImageField(upload_to="fotos/",null=True)
    publicacion_servicio = models.ForeignKey(Publicacion_Servicio,on_delete=models.CASCADE)

    def __str__(self):
        return "Foto_Servicio"  

class Calendario(models.Model):
    id_calendario = models.IntegerField(primary_key=True)
    relator = models.ForeignKey(Relator,on_delete=models.CASCADE)

    def __str__(self):
        return "Calendario"

class Dia_trabajo(models.Model):
    id_dia_trabajo = models.IntegerField(primary_key=True)
    hora_inicio = models.TimeField(null=False)
    hora_fin = models.TimeField(null=False)
    tarea = models.CharField(max_length=100,null=False)
    calendario = models.ForeignKey(Calendario,on_delete=models.CASCADE)


    def __str__(self):
        return "Dia_trabajo"