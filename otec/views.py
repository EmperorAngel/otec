from django.shortcuts import render
from .models import Relator,Otec,Empresa_Servicio,Publicacion,Foto_Publicacion
from django.shortcuts import redirect
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout,login as auth_login
from django.contrib.auth.decorators import login_required,permission_required

# Create your views here.

def index(request):
    return render(request,'index.html',{'prueba':'otec'})

def login(request):
    return render(request,'login.html',{'prueba':'xd'})

def registro(request):
    return render(request,'registro.html',{})

def login_iniciar(request):
    username = request.POST.get('user','')
    contrasenia = request.POST.get('password','')
    print(username+" "+contrasenia)
    user = authenticate(request,username=username,password=contrasenia)
    print(user)
    if user is not None:
        print("way1")
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("catalog")
    else:
        print("way2")
        return redirect("login")

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

def crearRelator(request):

    rut = request.POST.get('rut','')
    nombre= request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    telefono = request.POST.get('telefono',0)
    email = request.POST.get('email','')
    contrasenia = request.POST.get('contrasenia','')

    relator = Relator(rut_relator=rut,nombre=nombre,apellido=apellido,telefono=telefono,email=email)

    u= User.objects.create_user(username=relator.rut_relator,password=contrasenia,first_name=relator.nombre,last_name=relator.apellido,email=relator.email)
    u.save()
    relator.save()
    return redirect('login')

@login_required(login_url='login')
def cargaEditar(request,id):
    relator = Relator.objects.get(pk=id)
    usuario = request.session.get('usuario',None)
    return render(request,'editarRelator.html',{'relator':relator},{'usuario':usuario})

@login_required(login_url='login')
def cargaEditar2(request):
    success = False
    relator={}
    user = request.user
    if(request.method == 'POST'):
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Relator.objects.filter(rut_relator=user.username).exists()):
                print("userexist")
                rela = Relator.objects.get(rut_relator=user.username)
                relator['rut']=rela.rut_relator
                relator['nombre']=rela.nombre
                relator['apellido']=rela.apellido
                relator['email']=rela.email
                relator['telefono']=rela.telefono
                success = True
    return JsonResponse({'success': success,'relator':relator})


@login_required(login_url='login')
def editarRelator(request,id):
    relator = Relator.objects.get(pk = id)
    telefono = request.POST.get('telefono','')
    email = request.POST.get('email','')
    foto = request.FILES.get('foto',Relator.foto)
    
    relator.telefono = nombre
    relator.email = email
    relator.foto = foto
    relator.save()
    return redirect('menurelator')

@login_required(login_url='login')
def editarRelator2(request):
    success = False
    telefono = request.POST.get('telefono','')
    email = request.POST.get('email','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    foto = request.POST.get('foto',False)

    user = request.user
    if(request.method == 'POST'):
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Relator.objects.filter(rut_relator=user.username).exists()):
                print("userexist")
                rela = Relator.objects.get(rut_relator=user.username)
                if(telefono!=''):
                    rela.telefono=telefono
                if(email!=''):
                    rela.email=email
                if(nombre!=''):
                    rela.nombre=nombre
                if(apellido!=''):
                    rela.apellido=apellido
                if(foto!=False):
                    print('subio')
                    rela.foto=foto
                rela.save()
                
                success = True
    return JsonResponse({'success': success,'relator':rela.rut_relator})

def deshabilitarRelator(request):
    relator = Relator.objects.get(pk = id)
    estado = ('Inactivo',False)
    relator.estado = estado
    relator.save()
    return redirect('menurelator')

@login_required(login_url='login')
def deshabilitarRelator2(request):
    success=False
    if(request.method == 'POST'):
        user = request.user
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Relator.objects.filter(rut_relator=user.username).exists()):
                print("userexist")
                rela = Relator.objects.get(rut_relator=user.username)
                rela.estado = False
                rela.save()
                success=True

    return JsonResponse({'success': success,'relator':rela.rut_relator})

def catalog(request):
    user = request.user
    if(not user.is_anonymous):
        rela = Relator.objects.get(rut_relator=user.username)
        return render(request,'catalog.html',{'foto':rela.foto})
    else:
        return render(request,'catalog.html',{})

##publicaciones xd
def publicaciones(request):
    usuario = request.session.get('usuario',None)
    return render(request,'publicaciones.html', {'publicaciones': Publicacion.objects.all(),'fotos': Foto_Publicacion.objects.all(), 'usuario':usuario})


@login_required(login_url='login')
def crearPublicacion(request):

    titulo = request.POST.get('titulo','')
    fecha_inicio= request.POST.get('fechaini','')
    fecha_termino = request.POST.get('fechater','')
    sueldo = request.POST.get('sueldo',0)
    foto = request.POST.get('fotopubli',False)

    publi = Publicacion(titulo=titulo,fecha_inicio=fecha_inicio,fecha_termino=fecha_termino,sueldo=sueldo)
    foto= Foto_Publicacion(foto=foto)

    publi.save()
    foto.save()
return render(request,'publicaciones.html',{})
##nose como se va llamar el html donde veran las publicaciones 



@login_required(login_url='login')
def eliminarPublicacion(request,id):
    if(request.method == 'POST'):
        user = request.user
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Publicacion.objects.filter(id_publicacion=id).exists()):
                print("publiexist")
                publi = Publicacion.objects.get(pk = id)
                publi.delete()
    return redirect('publicaciones')

def cargaEditarPublicacion(request,id):
    success = False
    publicacion={}
    user = request.user
    if(request.method == 'POST'):
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Otec.objects.filter(rut_otec=user.username).exists()):
                print("userexist")
                publicacion = Publicacion.objects.get(rut_relator=user.username)
                publicacion['titulo']=publicacion.titulo
                publicacion['fecha_inicio']=publicacion.fecha_inicio
                publicacion['fecha_termino']=publicacion.fecha_termino
                publicacion['sueldo']=publicacion.sueldo
                success = True
    return JsonResponse({'success': success,'publicacion':publicacion})   
    
 @login_required(login_url='login')
def EditarPublicacion(request,id):
    success = False
    titulo = request.POST.get('titulo','')
    fecha_inicio = request.POST.get('fecha_inicio','')
    fecha_termino = request.POST.get('fecha_termino','')
    sueldo = request.POST.get('sueldo','')

    user = request.user
    if(request.method == 'POST'):
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Otec.objects.filter(rut_otec=user.username).exists()):
                print("userexist")
                publi = Publicacion.objects.get(id_publicacion=id)
                if(titulo!=''):
                    publi.titulo=titulo
                if(fecha_inicio!=''):
                    publi.fecha_inicio=fecha_inicio
                if(fecha_termino!=''):
                    publi.fecha_termino=fecha_termino
                if(sueldo!=''):
                    publi.sueldo=sueldo
                publi.save()
                
                success = True
    return JsonResponse({'success': success,'publicacion':publi.id_publicacion})   

##foto
def cargaEditarFoto(request,id):
    success = False
    publicacion={}
    user = request.user
    if(request.method == 'POST'):
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Otec.objects.filter(rut_otec=user.username).exists()):
                print("userexist")
                fotopublicacion = Foto_Publicacion.objects.get(id_foto=id)
                fotopublicacion['foto']=fotopublicacion.foto
                success = True
    return JsonResponse({'success': success,'fotopublicacion':fotopublicacion})   
    
 @login_required(login_url='login')
def EditarFoto(request,id):
    success = False
    foto = request.POST.get('foto',False)

    user = request.user
    if(request.method == 'POST'):
        print('postrequest')
        if(not user.is_anonymous):
            print("userlogged")
            if(Otec.objects.filter(rut_otec=user.username).exists()):
                print("userexist")
                fotopublicacion = Foto_Publicacion.objects.get(id_foto=id)
                if(foto!=False):
                    fotopublicacion.foto=foto
                publi.save()
                success = True
    return JsonResponse({'success': success,'fotopublicacion':fotopublicacion.id_foto}) 

















        