# Generated by Django 2.1.2 on 2019-05-07 22:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('otec', '0007_auto_20190504_2206'),
    ]

    operations = [
        migrations.AddField(
            model_name='relator',
            name='estado',
            field=models.BooleanField(default=True, verbose_name='Activo'),
        ),
    ]
