# Generated by Django 2.1.2 on 2019-05-05 02:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('otec', '0005_auto_20190504_2015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relator',
            name='certificacion_id_certificacion',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='otec.Certificacion'),
        ),
        migrations.AlterField(
            model_name='relator',
            name='estado_id_estado',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='otec.Estado'),
        ),
    ]
