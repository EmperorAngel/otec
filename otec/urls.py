from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path,include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('login/registro/', views.registro, name='registro'),
    path('login/registro/crearrelator',views.crearRelator,name="crearRelator"),
    path('catalog', views.catalog, name='catalog'),
    path('cargaEditar2', views.cargaEditar2, name='cargaEditar2'),
    path('editarRelator2', views.editarRelator2, name='editarRelator2'),
    path('deshabilitarRelator2', views.deshabilitarRelator2, name='deshabilitarRelator2'),

    ]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)